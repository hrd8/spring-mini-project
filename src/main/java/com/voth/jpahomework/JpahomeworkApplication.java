package com.voth.jpahomework;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JpahomeworkApplication {

	public static void main(String[] args) {
		SpringApplication.run(JpahomeworkApplication.class, args);
	}

}

