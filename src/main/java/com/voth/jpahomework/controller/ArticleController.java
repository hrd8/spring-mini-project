package com.voth.jpahomework.controller;

import com.voth.jpahomework.model.dto.ArticleDto;
import com.voth.jpahomework.service.ArticleService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Tag(name = "5 Articles")
@RestController
@RequestMapping("/api/v1/articles")
public class ArticleController {
  private final ArticleService articleService;

  @Autowired
  public ArticleController(ArticleService articleService) {
    this.articleService = articleService;
  }

  @Operation(
      operationId = "findAllArticle",
      description = "get all article",
      summary = "findAllArticle")
  @GetMapping()
  public ResponseEntity<Object> findAll(
      @RequestParam(name = "page", defaultValue = "0", required = false) String page,
      @RequestParam(name = "offset", defaultValue = "5", required = false) String offset) {
    return articleService.findAll(page, offset);
  }

  @Operation(
      operationId = "findAllArticleByUserId",
      description = "get all article By user's id",
      summary = "findAllArticleByUserId")
  @GetMapping("users/{userId}")
  public ResponseEntity<Object> findAllByUserId(
      @PathVariable String userId,
      @RequestParam(name = "page", defaultValue = "0", required = false) String page,
      @RequestParam(name = "offset", defaultValue = "5", required = false) String offset) {
    return articleService.findAllByUserId(userId, page, offset);
  }

  @Operation(
      operationId = "search",
      description =
          "Search for Article </br> "
              + "Syntax: fieldName:criteria </br> "
              + "(criteria is optional. Available criteria is OR,AND) </br> "
              + "if not specified it default to OR criteria </br>"
              + "example: </br> "
              + "1:or(userId) </br> "
              + "voth:and(username) </br> "
              + "this translated to where u.name like '%voth%' or u.id = 1 </br> "
              + "it work in the order of </br> "
              + "username >> article title >> category name >> articleDescription >> category id >> user id",
      summary = "ArticleSearch")
  @GetMapping("search")
  public ResponseEntity<Object> search(
      @RequestParam(name = "userName", required = false) String userName,
      @RequestParam(name = "articleTitle", required = false) String articleTitle,
      @RequestParam(name = "categoryName", required = false) String categoryName,
      @RequestParam(name = "articleDescription", required = false) String articleDescription,
      @RequestParam(name = "categoryId", required = false) String categoryId,
      @RequestParam(name = "userId", required = false) String userId,
      @RequestParam(name = "page", defaultValue = "0", required = false) String page,
      @RequestParam(name = "offset", defaultValue = "5", required = false) String offset) {
    return articleService.search(
        articleTitle, userName, categoryName, articleDescription, categoryId, userId, page, offset);
  }

  @Operation(
      operationId = "findAllArticleByCategoryId",
      description = "get all article By category's id",
      summary = "findAllArticleByCategoryId")
  @GetMapping("categories/{categoryId}")
  public ResponseEntity<Object> findAllByCategoryId(
      @PathVariable String categoryId,
      @RequestParam(name = "page", defaultValue = "0", required = false) String page,
      @RequestParam(name = "offset", defaultValue = "5", required = false) String offset) {
    return articleService.findAllByCategoryId(categoryId, page, offset);
  }

  @Operation(
      operationId = "findAllArticleById",
      description = "get all article By id",
      summary = "findAllArticleById")
  @GetMapping("{id}")
  public ResponseEntity<Object> findById(@PathVariable String id) {
    return articleService.findById(id);
  }

  @Operation(
      operationId = "createArticle",
      description = "Create new Article",
      summary = "createArticle")
  @PostMapping()
  public ResponseEntity<Object> create(
      @Valid @RequestBody ArticleDto articleDto,
      BindingResult bindingResult,
      @RequestParam(name = "userId") String userId,
      @RequestParam(name = "categoryId", required = false) String categoryId) {
    return articleService.create(articleDto, bindingResult, userId, categoryId);
  }

  @Operation(
      operationId = "updateArticleById",
      description = "update article By id",
      summary = "updateArticleById")
  @PutMapping("{id}")
  public ResponseEntity<Object> update(
      @Valid @RequestBody ArticleDto articleDto,
      BindingResult bindingResult,
      @PathVariable String id) {
    return articleService.update(articleDto, bindingResult, id);
  }

  @Operation(
      operationId = "deleteArticleById",
      description = "delete article By id",
      summary = "deleteArticleById")
  @DeleteMapping("{id}")
  public ResponseEntity<Object> delete(@PathVariable String id) {
    return articleService.delete(id);
  }
}
