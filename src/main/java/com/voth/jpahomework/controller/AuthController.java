package com.voth.jpahomework.controller;

import com.voth.jpahomework.model.dto.UserDto;
import com.voth.jpahomework.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Tag(name = "1 Authentication")
@RestController
@RequestMapping("/api/v1/auth")
public class AuthController {
  private final UserService userService;

  @Autowired
  public AuthController(UserService userService) {
    this.userService = userService;
  }

  @Operation(operationId = "login", description = "log in", summary = "login")
  @PostMapping("login")
  public ResponseEntity<Object> login(
      @Valid @RequestBody UserDto userDto, BindingResult bindingResult) {
    return userService.login(userDto, bindingResult);
  }

  @Operation(operationId = "createUser", description = "create new user", summary = "createUser")
  @PostMapping()
  public ResponseEntity<Object> create(
      @Valid @RequestBody UserDto userDto,
      BindingResult bindingResult,
      @RequestParam(name = "roleId") String roleId) {
    return userService.create(userDto, bindingResult, roleId);
  }
}
