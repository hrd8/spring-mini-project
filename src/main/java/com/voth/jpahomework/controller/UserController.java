package com.voth.jpahomework.controller;

import com.voth.jpahomework.model.dto.UserDto;
import com.voth.jpahomework.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Tag(name = "3 Users")
@RestController
@RequestMapping("/api/v1/users")
public class UserController {

  private final UserService userService;

  @Autowired
  public UserController(UserService userService) {
    this.userService = userService;
  }

  @Operation(operationId = "findAllUsers", description = "find all users", summary = "findAllUsers")
  @GetMapping()
  public ResponseEntity<Object> findAll(
      @RequestParam(name = "page", defaultValue = "0", required = false) String page,
      @RequestParam(name = "offset", defaultValue = "5", required = false) String offset) {
    return userService.findAll(page, offset);
  }

  @Operation(
      operationId = "findAllByRoleId",
      description = "find users by role's id",
      summary = "findAllByRoleId")
  @GetMapping("roles/{roleId}")
  public ResponseEntity<Object> findAllByRoleId(
      @PathVariable String roleId,
      @RequestParam(name = "page", defaultValue = "0", required = false) String page,
      @RequestParam(name = "offset", defaultValue = "5", required = false) String offset) {
    return userService.findByRoleId(roleId, page, offset);
  }

  @Operation(
      operationId = "findUserById",
      description = "find user By id",
      summary = "findUserById")
  @GetMapping("{id}")
  public ResponseEntity<Object> findById(@PathVariable String id) {
    return userService.findById(id);
  }



  @Operation(
      operationId = "updateUserById",
      description = "update User By id",
      summary = "updateUserById")
  @PutMapping("{id}")
  public ResponseEntity<Object> update(
      @Valid @RequestBody UserDto userDto, BindingResult bindingResult, @PathVariable String id) {
    return userService.update(userDto, bindingResult, id);
  }

  @Operation(
      operationId = "deleteUserById",
      description = "delete User By id",
      summary = "deleteUserById")
  @DeleteMapping("{id}")
  public ResponseEntity<Object> delete(@PathVariable String id) {
    return userService.delete(id);
  }

}
