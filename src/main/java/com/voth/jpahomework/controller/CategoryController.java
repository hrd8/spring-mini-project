package com.voth.jpahomework.controller;

import com.voth.jpahomework.model.dto.CategoryDto;
import com.voth.jpahomework.service.CategoryService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Tag(name = "4 Categories")
@RestController
@RequestMapping("/api/v1/categories")
public class CategoryController {
  private final CategoryService categoryService;

  @Autowired
  public CategoryController(CategoryService categoryService) {
    this.categoryService = categoryService;
  }

  @Operation(
      operationId = "findAllCategories",
      description = "find all categories",
      summary = "findAllCategories")
  @GetMapping()
  public ResponseEntity<Object> findAll(
      @RequestParam(name = "page", defaultValue = "0", required = false) String page,
      @RequestParam(name = "offset", defaultValue = "5", required = false) String offset) {
    return categoryService.findAll(page, offset);
  }

  @Operation(
      operationId = "findCategoryById",
      description = "find Category By id",
      summary = "findCategoryById")
  @GetMapping("{id}")
  public ResponseEntity<Object> findById(@PathVariable String id) {
    return categoryService.findById(id);
  }

  @Operation(
      operationId = "createCategory",
      description = "create new category",
      summary = "createCategory")
  @PostMapping()
  public ResponseEntity<Object> create(
      @Valid @RequestBody CategoryDto categoryDto, BindingResult bindingResult) {
    return categoryService.create(categoryDto, bindingResult);
  }

  @Operation(
      operationId = "updateCategoryById",
      description = "update category By id",
      summary = "updateCategoryById")
  @PutMapping("{id}")
  public ResponseEntity<Object> update(
      @Valid @RequestBody CategoryDto categoryDto,
      BindingResult bindingResult,
      @PathVariable String id) {
    return categoryService.update(categoryDto, bindingResult, id);
  }

  @Operation(
      operationId = "deleteCategoryById",
      description = "delete category By id",
      summary = "deleteCategoryById")
  @DeleteMapping("{id}")
  public ResponseEntity<Object> delete(@PathVariable String id) {
    return categoryService.delete(id);
  }
}
