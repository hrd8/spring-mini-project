package com.voth.jpahomework.controller;

import com.voth.jpahomework.model.dto.RoleDto;
import com.voth.jpahomework.service.RoleService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
@Tag(name = "2 Roles")
@RestController
@RequestMapping("/api/v1/roles")
public class RoleController {
  private final RoleService roleService;

  @Autowired
  public RoleController(RoleService roleService) {
    this.roleService = roleService;
  }

  @Operation(operationId = "findAllRole", description = "find all role", summary = "findAllRole")
  @GetMapping()
  public ResponseEntity<Object> findAll(
      @RequestParam(name = "page", defaultValue = "0", required = false) String page,
      @RequestParam(name = "offset", defaultValue = "5", required = false) String offset) {
    return roleService.findAll(page, offset);
  }

  @Operation(
      operationId = "FindRoleById",
      description = "find role By id",
      summary = "findRoleById")
  @GetMapping("{id}")
  public ResponseEntity<Object> findById(@PathVariable String id) {
    return roleService.findById(id);
  }

  @Operation(operationId = "createRole", description = "create new role", summary = "createRole")
  @PostMapping()
  public ResponseEntity<Object> create(
      @Valid @RequestBody RoleDto roleDto, BindingResult bindingResult) {
    return roleService.create(roleDto, bindingResult);
  }

  @Operation(
      operationId = "updateRoleById",
      description = "update role By id",
      summary = "updateRoleById")
  @PutMapping("{id}")
  public ResponseEntity<Object> update(
      @Valid @RequestBody RoleDto roleDto, BindingResult bindingResult, @PathVariable String id) {
    return roleService.update(roleDto, bindingResult, id);
  }

  @Operation(
      operationId = "deleteRoleById",
      description = "delete Role By id",
      summary = "deleteRoleById")
  @DeleteMapping("{id}")
  public ResponseEntity<Object> delete(@PathVariable String id) {
    return roleService.delete(id);
  }
}
