package com.voth.jpahomework.config;

import com.voth.jpahomework.model.response.BaseErrorResponse;
import io.swagger.v3.core.converter.AnnotatedType;
import io.swagger.v3.core.converter.ModelConverters;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.media.Content;
import io.swagger.v3.oas.models.media.ObjectSchema;
import io.swagger.v3.oas.models.media.Schema;
import io.swagger.v3.oas.models.responses.ApiResponse;
import io.swagger.v3.oas.models.responses.ApiResponses;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springdoc.core.customizers.OpenApiCustomiser;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.filter.ForwardedHeaderFilter;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class SwaggerConfiguration {
  private static final String COMPONENTS_SCHEMAS_REF = "#/components/schemas/";
  private static final String COMPONENTS_RESPONSES_REF = "#/components/responses/";
  private static final String BAD_REQUEST_RESPONSE_REF = "BadRequest";
  private static final String INTERNAL_SERVER_ERROR_RESPONSE_REF = "InternalServerError";
  private static final String FORBIDEN_RESPONSE_REF = "Forbiden";

  @Bean
  ForwardedHeaderFilter forwardedHeaderFilter() {
    return new ForwardedHeaderFilter();
  }

  @Bean
  public OpenApiCustomiser openApiCustomiser() {
    return openApi -> {
      var schemas = openApi.getComponents().getSchemas();
      schemas.putAll(this.createSchema());
      openApi
          .getPaths()
          .values()
          .forEach(
              pathItem ->
                  pathItem
                      .readOperations()
                      .forEach(
                          operation -> this.addMissingDefaultResponses(operation.getResponses())));
    };
  }
  /** Add default global response if it is missing. */
  private void addMissingDefaultResponses(ApiResponses apiResponses) {
    apiResponses.putIfAbsent(
        "400", new ApiResponse().$ref(COMPONENTS_RESPONSES_REF + BAD_REQUEST_RESPONSE_REF));
    apiResponses.putIfAbsent(
        "403", new ApiResponse().$ref(COMPONENTS_RESPONSES_REF + FORBIDEN_RESPONSE_REF));
    apiResponses.putIfAbsent(
        "500",
        new ApiResponse().$ref(COMPONENTS_RESPONSES_REF + INTERNAL_SERVER_ERROR_RESPONSE_REF));
  }

  /**
   * Create all related schemas of a specific model class.
   *
   * @return Generated schemas.
   */
  @SuppressWarnings("rawtypes")
  private Map<String, Schema> createSchema() {
    return ModelConverters.getInstance()
        .resolveAsResolvedSchema(new AnnotatedType(BaseErrorResponse.class))
        .referencedSchemas;
  }

  /**
   * Generate reusable responses.
   *
   * @return Generated reusable responses.
   */
  private Map<String, ApiResponse> getReusableResponse() {
    Map<String, ApiResponse> responseMap = new HashMap<>();
    responseMap.put(
        FORBIDEN_RESPONSE_REF,
        this.createApiErrorResponse(
            HttpStatus.FORBIDDEN, FORBIDEN_RESPONSE_REF, "FORBIDEN", FORBIDEN_RESPONSE_REF));
    responseMap.put(
        BAD_REQUEST_RESPONSE_REF,
        this.createApiErrorResponse(
            HttpStatus.BAD_REQUEST, "Bad request", "Bad request", "Some Error Occurred"));
    responseMap.put(
        INTERNAL_SERVER_ERROR_RESPONSE_REF,
        this.createApiErrorResponse(
            HttpStatus.INTERNAL_SERVER_ERROR,
            "Unexpected error",
            "Internal server error",
            "Internal Server Error"));
    return responseMap;
  }

  /**
   * Create a new error {@link ApiResponse} object of a specific response status.
   *
   * @param httpStatus Response status.
   * @param errorMessage Response's error messages.
   * @param responseDescription Response description.
   * @param error Error's object
   * @param <T> Error's Object Type
   * @return {@link ApiResponse}.
   */
  private <T> ApiResponse createApiErrorResponse(
      HttpStatus httpStatus, String errorMessage, String responseDescription, T error) {
    var schema = new ObjectSchema().$ref(COMPONENTS_SCHEMAS_REF + "BaseErrorResponse");
    var mediaType =
        new io.swagger.v3.oas.models.media.MediaType()
            .schema(schema)
            .example(new BaseErrorResponse<>(httpStatus.value(), errorMessage, new Date(), error));
    var content = new Content().addMediaType(MediaType.APPLICATION_JSON_VALUE, mediaType);
    return new ApiResponse().content(content).description(responseDescription);
  }

  @Bean
  public OpenAPI customOpenAPI() {
    final String securitySchemeName = "bearerAuth";
    return new OpenAPI()
        .addSecurityItem(new SecurityRequirement().addList(securitySchemeName))
        .components(
            new Components()
                .responses(this.getReusableResponse())
                .addSecuritySchemes(
                    securitySchemeName,
                    new SecurityScheme()
                        .name(securitySchemeName)
                        .type(SecurityScheme.Type.HTTP)
                        .scheme("bearer")
                        .bearerFormat("JWT")))
        .info(
            new Info()
                .title("Article API")
                .version("1.0")
                .description("Api Documentation Of Article Api"));
  }
}
