package com.voth.jpahomework.config;

import com.voth.jpahomework.model.Roles;
import com.voth.jpahomework.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class PostSetUpConfig {
  private final RoleRepository roleRepository;

  @Autowired
  public PostSetUpConfig(RoleRepository roleRepository) {
    this.roleRepository = roleRepository;
  }

  @PostConstruct
  public void init() {
    if (roleRepository.findByName("admin".toUpperCase()).isEmpty()) {
      roleRepository.save(new Roles("admin"));
    }
  }
}
