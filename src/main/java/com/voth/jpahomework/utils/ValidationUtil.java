package com.voth.jpahomework.utils;

import com.voth.jpahomework.model.ValidationModel;
import com.voth.jpahomework.model.dto.CategoryDto;
import com.voth.jpahomework.model.dto.UserDto;
import com.voth.jpahomework.model.payload.ErrorPayload;
import com.voth.jpahomework.model.response.BaseErrorResponse;
import com.voth.jpahomework.repository.CategoryRepository;
import com.voth.jpahomework.repository.UserRepository;
import com.voth.jpahomework.repository.criteria.ArticleSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Component
public class ValidationUtil {

  private final UserRepository userRepository;
  private final CategoryRepository categoryRepository;

  @Autowired
  public ValidationUtil(UserRepository userRepository, CategoryRepository categoryRepository) {
    this.userRepository = userRepository;
    this.categoryRepository = categoryRepository;
  }

  public ResponseEntity<Object> getObjectResponseEntity(BindingResult bindingResult) {
    List<ErrorPayload> errors = new ArrayList<>();
    return getBindingResultError(bindingResult, errors);
  }

  /**
   * this method is used to validate Conversion between String and Long to make sure that there is
   * no exception and not negative
   *
   * @param str Array of {@link ValidationModel} can not be null;
   * @return list of {@link ErrorPayload}
   */
  public List<ErrorPayload> numberValidationLogic(ValidationModel... str) {
    List<ErrorPayload> errors = new ArrayList<>();
    Arrays.stream(str)
        .forEach(
            s -> {
              if (s.getValue() != null && s.getValue().split(":").length > 1) {
                try {
                  String[] localSplit = s.getValue().split(":");
                  long localVar = Long.parseLong(localSplit[0]);
                  if (localVar < 0) {
                    errors.add(new ErrorPayload(s.getFieldName(), "Value Can't Be Negative"));
                  }
                } catch (Exception e) {
                  errors.add(
                      new ErrorPayload(s.getFieldName(), "Invalid Input: Input Must Be A Number"));
                }
              } else if (s.getValue() != null){
                try {
                  long localVar = Long.parseLong(s.getValue());
                  if (localVar < 0) {
                    errors.add(new ErrorPayload(s.getFieldName(), "Value Can't Be Negative"));
                  }
                } catch (Exception e) {
                  errors.add(
                      new ErrorPayload(s.getFieldName(), "Invalid Input: Input Must Be A Number"));
                }
              }
            });
    return errors;
  }

  /**
   * this method is used to add validation Error from Binding result on top of the given Error in
   * argument
   *
   * @param bindingResult can not be null
   * @param errors list of {@link ErrorPayload} can be empty
   * @return null if error not exist,otherwise {@link ResponseEntity} object
   */
  private ResponseEntity<Object> getBindingResultError(
      BindingResult bindingResult, List<ErrorPayload> errors) {
    if (bindingResult.hasErrors()) {
      bindingResult
          .getFieldErrors()
          .forEach(
              fieldError ->
                  errors.add(
                      new ErrorPayload(fieldError.getField(), fieldError.getDefaultMessage())));
      return new ResponseEntity<>(
          new BaseErrorResponse<>(
              400, HttpStatus.BAD_REQUEST.getReasonPhrase(), new Date(), errors),
          HttpStatus.BAD_REQUEST);
    }
    return null;
  }

  /**
   * this method used to get validation's error from {@link BindingResult} and validate if user
   * exist
   *
   * @param bindingResult can not be null
   * @param userDto data's Transfer object of User
   * @return {@link ResponseEntity} object
   */
  public ResponseEntity<Object> getObjectResponseEntity(
      BindingResult bindingResult, UserDto userDto) {
    List<ErrorPayload> errors = new ArrayList<>();
    if (userRepository.findByUsername(userDto.getUsername()).isPresent()) {
      errors.add(new ErrorPayload("username", "User Already Exist"));

      return new ResponseEntity<>(
          new BaseErrorResponse<>(
              400, HttpStatus.BAD_REQUEST.getReasonPhrase(), new Date(), errors),
          HttpStatus.BAD_REQUEST);
    }
    return getBindingResultError(bindingResult, errors);
  }

  /**
   * this method used to get validation's error from {@link BindingResult} and validate if user
   * exist
   *
   * @param bindingResult can not be null
   * @param categoryDto data's Transfer object of Category
   * @return {@link ResponseEntity} object
   */
  public ResponseEntity<Object> getObjectResponseEntity(
      BindingResult bindingResult, CategoryDto categoryDto) {
    List<ErrorPayload> errors = new ArrayList<>();
    if (categoryRepository.findByName(categoryDto.getName().toUpperCase()).isPresent()) {
      errors.add(new ErrorPayload("name", "Category Already Exist"));

      return new ResponseEntity<>(
          new BaseErrorResponse<>(
              400, HttpStatus.BAD_REQUEST.getReasonPhrase(), new Date(), errors),
          HttpStatus.BAD_REQUEST);
    }
    return getBindingResultError(bindingResult, errors);
  }

  /**
   * this method is used to get Response Entity object of the error from number Validation
   *
   * @param str Array of {@link ValidationModel} can not be null;
   * @return {@link ResponseEntity} object
   */
  public ResponseEntity<Object> numberValidation(ValidationModel... str) {
    List<ErrorPayload> errors = numberValidationLogic(str);
    if (!errors.isEmpty()) {
      return new ResponseEntity<>(
          new BaseErrorResponse<>(
              400, HttpStatus.BAD_REQUEST.getReasonPhrase(), new Date(), errors),
          HttpStatus.BAD_REQUEST);
    } else {
      return null;
    }
  }

  /**
   * this method used to get validation's error from {@link BindingResult} and validate if user
   * exist
   *
   * @param bindingResult can not be null
   * @param userId can not be null
   * @param idName can not be null
   * @return {@link ResponseEntity} object
   */
  public ResponseEntity<Object> getObjectResponseEntity(
      BindingResult bindingResult, String userId, String idName) {
    List<ErrorPayload> errors = numberValidationLogic(new ValidationModel(idName, userId));
    return getBindingResultError(bindingResult, errors);
  }

  /**
   * @param list default specification list
   * @param data varArg for search criteria (name:value)
   * @return list of ArticleSpecification
   */
  public List<ArticleSpecification> specListGenerator(
      List<ArticleSpecification> list, String... data) {
    Arrays.stream(data)
        .forEach(
            val -> {
              if (val.split(":").length > 1) {
                String[] split = val.split(":");
                if (!split[1].equalsIgnoreCase("null")) {
                  list.add(new ArticleSpecification(val));
                }
              }
            });
    return list;
  }

  /**
   * return true if it used AND criteria otherwise false
   *
   * @param val can not be null
   * @return {@link boolean}
   */
  public boolean isUsingAndCriteria(String val) {
    String[] tmp = val.split(":");
    return tmp.length == 3
        && tmp[2] != null
        && (tmp[2].equalsIgnoreCase("and") || tmp[2].equalsIgnoreCase("or"))
        && tmp[2].equalsIgnoreCase("and");
  }
}
