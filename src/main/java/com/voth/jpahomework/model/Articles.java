package com.voth.jpahomework.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.voth.jpahomework.model.dto.ArticleDto;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@AllArgsConstructor
public class Articles {
  @Id
  @Column(name = "id", nullable = false)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private String title;
  private String description;

  public Articles(ArticleDto articleDto, Users user, Categories categories) {
    this.title = articleDto.getTitle();
    this.description = articleDto.getDescription();
    this.category = categories;
    this.createBy = user;
  }

  @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
  @JsonManagedReference
  @ToString.Exclude
  private Categories category;

  @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
  @JsonManagedReference
  @ToString.Exclude
  private Users createBy;

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
    Articles articles = (Articles) o;
    return id != null && Objects.equals(id, articles.id);
  }

  @Override
  public int hashCode() {
    return 0;
  }
}
