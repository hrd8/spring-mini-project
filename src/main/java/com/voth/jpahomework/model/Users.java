package com.voth.jpahomework.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.voth.jpahomework.model.dto.UserDto;
import lombok.*;
import org.hibernate.Hibernate;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@AllArgsConstructor
public class Users {

  @Id
  @Column(name = "id", nullable = false)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private String username;
  @JsonIgnore private String password;

  public Users(UserDto userDto, Roles role, String encryptedPassword) {
    this.username = userDto.getUsername();
    this.password = encryptedPassword;
    this.roles.add(role);
  }
  public Users(Long id, String username,List<Roles> roles) {
    this.id = id;
    this.username = username;
    this.roles.addAll(roles);
  }

  @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
  @JoinTable(
      name = "user_role",
      joinColumns = @JoinColumn(name = "user_id"),
      inverseJoinColumns = @JoinColumn(name = "Role_id"))
  @ToString.Exclude
  @JsonManagedReference
  List<Roles> roles = new ArrayList<>();

  @OneToMany(mappedBy = "createBy", cascade = CascadeType.ALL, orphanRemoval = true)
  @JsonBackReference
  @ToString.Exclude
  List<Articles> articles = new ArrayList<>();



  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
    Users users = (Users) o;
    return id != null && Objects.equals(id, users.id);
  }

  @Override
  public int hashCode() {
    return 0;
  }
}
