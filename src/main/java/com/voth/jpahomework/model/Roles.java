package com.voth.jpahomework.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.voth.jpahomework.model.dto.RoleDto;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@AllArgsConstructor
public class Roles {
  @Id
  @Column(name = "id", nullable = false)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private String name;

  @ManyToMany(mappedBy = "roles", cascade = CascadeType.ALL)
  @ToString.Exclude
  @JsonBackReference
  private List<Users> users;

  public Roles(RoleDto roleDto) {
    this.name = roleDto.getName().toUpperCase();
  }
  public Roles(String name) {
    this.name = name.toUpperCase();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
    Roles roles = (Roles) o;
    return id != null && Objects.equals(id, roles.id);
  }

  @Override
  public int hashCode() {
    return 0;
  }
}
