package com.voth.jpahomework.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@AllArgsConstructor
@Data
public class ValidationModel {
  private String fieldName;
  private String value;
}
