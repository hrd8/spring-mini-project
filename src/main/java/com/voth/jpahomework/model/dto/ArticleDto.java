package com.voth.jpahomework.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.validation.constraints.NotBlank;

@RequiredArgsConstructor
@AllArgsConstructor
@Data
public class ArticleDto {
  @NotBlank(message = "Title Can't Be Blank")
  private String title;

  @NotBlank(message = "Description Can't Be Blank")
  private String description;
}
