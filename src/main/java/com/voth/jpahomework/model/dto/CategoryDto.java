package com.voth.jpahomework.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.validation.constraints.NotBlank;

@RequiredArgsConstructor
@AllArgsConstructor
@Data
public class CategoryDto {
  @NotBlank(message = "Name Can't Be Blank")
  private String name;
}
