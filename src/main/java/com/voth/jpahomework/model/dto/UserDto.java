package com.voth.jpahomework.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@RequiredArgsConstructor
@AllArgsConstructor
@Data
public class UserDto {
  @NotBlank(message = "Username Can't be Blank")
  private String username;

  @NotBlank(message = "password Can't be Blank")
  @Pattern(
      message = "Minimum eight characters, at least one letter and one number",
      regexp = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$")
  private String password;
}
