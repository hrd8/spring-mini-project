package com.voth.jpahomework.model.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Pagination {
  private int limit;
  private int currentPage;
  private int nextPage;
  private int previousPage;
  private int totalCount;
  private int totalPages;
  private int startPage;
  private int endPage;

  public Pagination(String page, String offset, int totalPages, long totalElements) {
    this.currentPage = Integer.parseInt(page);
    this.limit = Integer.parseInt(offset);
    this.totalCount = (int) totalElements;
    this.totalPages = totalPages;
    this.startPage = 0;
    this.endPage = totalPages == 0 ? 0 : totalPages - 1;
    this.nextPage = totalPages == 0 ? 0 : this.currentPage == this.endPage ? this.currentPage : this.currentPage + 1;
    this.previousPage = this.currentPage == 0 ? 0 : this.currentPage - 1;
  }
}
