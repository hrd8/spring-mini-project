package com.voth.jpahomework.model.response;

import com.voth.jpahomework.model.payload.Pagination;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseWithPagination extends BaseResponse<Object>{
    Pagination pagination;

    public ResponseWithPagination(int code, String message, Date requestTime, Object payload, Pagination pagination) {
        super(code, message, requestTime, payload);
        this.pagination = pagination;
    }
}
