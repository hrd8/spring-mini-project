package com.voth.jpahomework.model.response;

import com.voth.jpahomework.model.Users;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;

import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class LogInResponse extends BaseResponse<Users>{
    private String jwt;

    public LogInResponse(int code, String message, Date requestTime, Users payload, String jwt) {
        super(code, message, requestTime, payload);
        this.jwt = jwt;
    }
}
