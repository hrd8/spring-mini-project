package com.voth.jpahomework.model.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class BaseErrorResponse<T> {
  private int code;
  private String message;

  @JsonFormat(
      shape = JsonFormat.Shape.STRING,
      pattern = "yyyy-MM-dd@HH:mm:ss",
      timezone = "Asia/Phnom_Penh")
  private Date requestTime;

  private T error;
}
