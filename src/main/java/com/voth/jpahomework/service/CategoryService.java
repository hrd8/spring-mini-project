package com.voth.jpahomework.service;

import com.voth.jpahomework.model.Categories;
import com.voth.jpahomework.model.ValidationModel;
import com.voth.jpahomework.model.dto.CategoryDto;
import com.voth.jpahomework.model.payload.Pagination;
import com.voth.jpahomework.model.response.BaseErrorResponse;
import com.voth.jpahomework.model.response.BaseResponse;
import com.voth.jpahomework.model.response.ResponseWithPagination;
import com.voth.jpahomework.repository.CategoryRepository;
import com.voth.jpahomework.utils.ValidationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import java.util.Date;
import java.util.List;

@Service
public class CategoryService {

  private static final String SUCCESS_FETCH = "Category Retrieved SuccessFully";
  private static final String SUCCESS_UPDATE = "Category Updated SuccessFully";
  private static final String SUCCESS_DELETE = "Category Deleted SuccessFully";
  private static final String SUCCESS_CREATE = "Category Created SuccessFully";
  private static final String OFFSET = "offset";
  private static final String PAGE = "page";
  private static final String CATEGORY_ID = "Category's ID";
  private final CategoryRepository categoryRepository;
  private final ValidationUtil validationUtil;

  @Autowired
  public CategoryService(CategoryRepository categoryRepository, ValidationUtil validationUtil) {
    this.categoryRepository = categoryRepository;
    this.validationUtil = validationUtil;
  }

  public ResponseEntity<Object> findAll(String page, String offset) {
    List<Categories> categories;
    Page<Categories> pageObj;
    try {
      ResponseEntity<Object> errors =
          validationUtil.numberValidation(
              new ValidationModel(PAGE, page), new ValidationModel(OFFSET, offset));
      if (errors != null) return errors;
      Pageable pageable = PageRequest.of(Integer.parseInt(page), Integer.parseInt(offset));
      pageObj = categoryRepository.findAll(pageable);
      categories = pageObj.getContent();
    } catch (Exception e) {
      return new ResponseEntity<>(
          new BaseErrorResponse<>(
              400, HttpStatus.BAD_REQUEST.getReasonPhrase(), new Date(), e.getMessage()),
          HttpStatus.BAD_REQUEST);
    }
    return new ResponseEntity<>(
        new ResponseWithPagination(
            200,
            SUCCESS_FETCH,
            new Date(),
            categories,
            new Pagination(page, offset, pageObj.getTotalPages(), pageObj.getTotalElements())),
        HttpStatus.OK);
  }

  public ResponseEntity<Object> findById(String id) {
    Categories category;
    try {
      ResponseEntity<Object> errors =
          validationUtil.numberValidation(new ValidationModel(CATEGORY_ID, id));
      if (errors != null) return errors;
      category = categoryRepository.findById(Long.valueOf(id)).orElseThrow();
    } catch (Exception e) {
      return new ResponseEntity<>(
          new BaseErrorResponse<>(
              400, HttpStatus.BAD_REQUEST.getReasonPhrase(), new Date(), e.getMessage()),
          HttpStatus.BAD_REQUEST);
    }
    return new ResponseEntity<>(
        new BaseResponse<>(200, SUCCESS_FETCH, new Date(), category), HttpStatus.OK);
  }

  public ResponseEntity<Object> create(CategoryDto categoryDto, BindingResult bindingResult) {
    ResponseEntity<Object> errors1 =
        validationUtil.getObjectResponseEntity(bindingResult, categoryDto);
    if (errors1 != null) return errors1;
    Categories category = categoryRepository.save(new Categories(categoryDto));
    return new ResponseEntity<>(
        new BaseResponse<>(201, SUCCESS_CREATE, new Date(), category), HttpStatus.CREATED);
  }

  public ResponseEntity<Object> update(
      CategoryDto categoryDto, BindingResult bindingResult, String id) {
    ResponseEntity<Object> errors =
        validationUtil.getObjectResponseEntity(bindingResult, id, CATEGORY_ID);
    if (errors != null) return errors;
    Categories category;
    try {
      Categories foundCategory = categoryRepository.getById(Long.valueOf(id));
      foundCategory.setName(categoryDto.getName().toUpperCase());
      category = categoryRepository.save(foundCategory);
    } catch (Exception e) {
      return new ResponseEntity<>(
          new BaseErrorResponse<>(
              400, HttpStatus.BAD_REQUEST.getReasonPhrase(), new Date(), e.getMessage()),
          HttpStatus.BAD_REQUEST);
    }
    return new ResponseEntity<>(
        new BaseResponse<>(200, SUCCESS_UPDATE, new Date(), category), HttpStatus.OK);
  }

  public ResponseEntity<Object> delete(String id) {
    try {
      ResponseEntity<Object> errors =
          validationUtil.numberValidation(new ValidationModel(CATEGORY_ID, id));
      if (errors != null) return errors;
      categoryRepository.deleteById(Long.valueOf(id));
    } catch (Exception e) {
      return new ResponseEntity<>(
          new BaseErrorResponse<>(
              400, HttpStatus.BAD_REQUEST.getReasonPhrase(), new Date(), e.getMessage()),
          HttpStatus.BAD_REQUEST);
    }
    return new ResponseEntity<>(
        new BaseResponse<>(200, SUCCESS_DELETE, new Date(), HttpStatus.OK.getReasonPhrase()),
        HttpStatus.OK);
  }
}
