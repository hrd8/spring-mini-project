package com.voth.jpahomework.service;

import com.voth.jpahomework.model.Roles;
import com.voth.jpahomework.model.ValidationModel;
import com.voth.jpahomework.model.dto.RoleDto;
import com.voth.jpahomework.model.payload.Pagination;
import com.voth.jpahomework.model.response.BaseErrorResponse;
import com.voth.jpahomework.model.response.BaseResponse;
import com.voth.jpahomework.model.response.ResponseWithPagination;
import com.voth.jpahomework.repository.RoleRepository;
import com.voth.jpahomework.utils.ValidationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class RoleService {

  private static final String SUCCESS_FETCH = "Role Retrieved SuccessFully";
  private static final String SUCCESS_UPDATE = "Role Updated SuccessFully";
  private static final String SUCCESS_DELETE = "Role Deleted SuccessFully";
  private static final String SUCCESS_CREATE = "Role Created SuccessFully";
  private static final String OFFSET = "offset";
  private static final String PAGE = "page";
  private static final String ROLE_ID = "Role's ID";
  private final RoleRepository roleRepository;
  private final ValidationUtil validationUtil;

  @Autowired
  public RoleService(RoleRepository roleRepository, ValidationUtil validationUtil) {
    this.roleRepository = roleRepository;
    this.validationUtil = validationUtil;
  }

  public ResponseEntity<Object> findAll(String page, String offset) {
    List<Roles> roles;
    Page<Roles> pageObj;
    try {
      ResponseEntity<Object> errors =
          validationUtil.numberValidation(
              new ValidationModel(PAGE, page), new ValidationModel(OFFSET, offset));
      if (errors != null) return errors;
      Pageable pageable = PageRequest.of(Integer.parseInt(page), Integer.parseInt(offset));
      pageObj = roleRepository.findAll(pageable);
      roles = pageObj.getContent();
    } catch (Exception e) {
      return new ResponseEntity<>(
          new BaseErrorResponse<>(
              400, HttpStatus.BAD_REQUEST.getReasonPhrase(), new Date(), e.getMessage()),
          HttpStatus.BAD_REQUEST);
    }
    return new ResponseEntity<>(
        new ResponseWithPagination(
            200,
            SUCCESS_FETCH,
            new Date(),
            roles,
            new Pagination(page, offset, pageObj.getTotalPages(), pageObj.getTotalElements())),
        HttpStatus.OK);
  }

  public ResponseEntity<Object> findById(String id) {
    Roles role;
    try {
      ResponseEntity<Object> errors =
          validationUtil.numberValidation(new ValidationModel(ROLE_ID, id));
      if (errors != null) return errors;
      role = roleRepository.findById(Long.valueOf(id)).orElseThrow();
    } catch (Exception e) {
      return new ResponseEntity<>(
          new BaseErrorResponse<>(
              400, HttpStatus.BAD_REQUEST.getReasonPhrase(), new Date(), e.getMessage()),
          HttpStatus.BAD_REQUEST);
    }
    return new ResponseEntity<>(
        new BaseResponse<>(200, SUCCESS_FETCH, new Date(), role), HttpStatus.OK);
  }

  public ResponseEntity<Object> create(RoleDto roleDto, BindingResult bindingResult) {
    ResponseEntity<Object> errors1 = validationUtil.getObjectResponseEntity(bindingResult);
    if (errors1 != null) return errors1;
    if (roleRepository.findByName(roleDto.getName().toUpperCase()).isPresent()) {
      return new ResponseEntity<>(
          new BaseErrorResponse<>(
              400, HttpStatus.BAD_REQUEST.getReasonPhrase(), new Date(), "Role Already Exist"),
          HttpStatus.BAD_REQUEST);
    }
    Roles role = roleRepository.save(new Roles(roleDto));
    return new ResponseEntity<>(
        new BaseResponse<>(201, SUCCESS_CREATE, new Date(), role), HttpStatus.CREATED);
  }

  public ResponseEntity<Object> update(RoleDto roleDto, BindingResult bindingResult, String id) {
    ResponseEntity<Object> errors =
        validationUtil.getObjectResponseEntity(bindingResult, id, ROLE_ID);
    if (errors != null) return errors;
    Roles role;
    try {
      Roles foundRole = roleRepository.getById(Long.valueOf(id));
      foundRole.setName(roleDto.getName().toUpperCase());
      role = roleRepository.save(foundRole);
    } catch (Exception e) {
      return new ResponseEntity<>(
          new BaseErrorResponse<>(
              400, HttpStatus.BAD_REQUEST.getReasonPhrase(), new Date(), e.getMessage()),
          HttpStatus.BAD_REQUEST);
    }
    return new ResponseEntity<>(
        new BaseResponse<>(200, SUCCESS_UPDATE, new Date(), role), HttpStatus.OK);
  }

  public ResponseEntity<Object> delete(String id) {
    try {
      ResponseEntity<Object> errors =
          validationUtil.numberValidation(new ValidationModel(ROLE_ID, id));
      if (errors != null) return errors;
      roleRepository.deleteById(Long.valueOf(id));
    } catch (Exception e) {
      return new ResponseEntity<>(
          new BaseErrorResponse<>(
              400, HttpStatus.BAD_REQUEST.getReasonPhrase(), new Date(), e.getMessage()),
          HttpStatus.BAD_REQUEST);
    }
    return new ResponseEntity<>(
        new BaseResponse<>(200, SUCCESS_DELETE, new Date(), HttpStatus.OK.getReasonPhrase()),
        HttpStatus.OK);
  }

    public Optional<Roles> findByName(String name) {
      return roleRepository.findByName(name);
  }
}
