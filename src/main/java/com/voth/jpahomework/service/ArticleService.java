package com.voth.jpahomework.service;

import com.voth.jpahomework.model.Articles;
import com.voth.jpahomework.model.Categories;
import com.voth.jpahomework.model.Users;
import com.voth.jpahomework.model.ValidationModel;
import com.voth.jpahomework.model.dto.ArticleDto;
import com.voth.jpahomework.model.payload.Pagination;
import com.voth.jpahomework.model.response.BaseErrorResponse;
import com.voth.jpahomework.model.response.BaseResponse;
import com.voth.jpahomework.model.response.ResponseWithPagination;
import com.voth.jpahomework.repository.ArticleRepository;
import com.voth.jpahomework.repository.CategoryRepository;
import com.voth.jpahomework.repository.UserRepository;
import com.voth.jpahomework.repository.criteria.ArticleSpecification;
import com.voth.jpahomework.utils.ValidationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ArticleService {

  private static final String SUCCESS_FETCH = "Articles Retrieved SuccessFully";
  private static final String SUCCESS_UPDATE = "Articles Updated SuccessFully";
  private static final String SUCCESS_DELETE = "Articles Deleted SuccessFully";
  private static final String SUCCESS_CREATE = "Article Created SuccessFully";
  private static final String OFFSET = "offset";
  private static final String PAGE = "page";
  private static final String USER_ID = "User's ID";
  private static final String CATEGORY_ID = "Category's ID";
  private static final String ARTICLE_ID = "Article's ID";
  private final ArticleRepository articleRepository;
  private final UserRepository userRepository;
  private final CategoryRepository categoryRepository;
  private final ValidationUtil validationUtil;

  @Autowired
  public ArticleService(
      ArticleRepository articleRepository,
      UserRepository userRepository,
      CategoryRepository categoryRepository,
      ValidationUtil validationUtil) {
    this.articleRepository = articleRepository;
    this.userRepository = userRepository;
    this.categoryRepository = categoryRepository;
    this.validationUtil = validationUtil;
  }

  public ResponseEntity<Object> findAll(String page, String offset) {
    List<Articles> articles;
    Page<Articles> pageObj;
    try {
      ResponseEntity<Object> errors =
          validationUtil.numberValidation(
              new ValidationModel(PAGE, page), new ValidationModel(OFFSET, offset));
      if (errors != null) return errors;
      Pageable pageable = PageRequest.of(Integer.parseInt(page), Integer.parseInt(offset));

      pageObj = articleRepository.findAll(pageable);
      articles = pageObj.getContent();
    } catch (Exception e) {
      return new ResponseEntity<>(
          new BaseErrorResponse<>(
              400, HttpStatus.BAD_REQUEST.getReasonPhrase(), new Date(), e.getMessage()),
          HttpStatus.BAD_REQUEST);
    }
    return new ResponseEntity<>(
        new ResponseWithPagination(
            200,
            SUCCESS_FETCH,
            new Date(),
            articles,
            new Pagination(page, offset, pageObj.getTotalPages(), pageObj.getTotalElements())),
        HttpStatus.OK);
  }

  public ResponseEntity<Object> findAllByUserId(String userId, String page, String offset) {
    List<Articles> articles;
    Page<Articles> pageObj;
    try {
      ResponseEntity<Object> errors =
          validationUtil.numberValidation(
              new ValidationModel(PAGE, page),
              new ValidationModel(OFFSET, offset),
              new ValidationModel(USER_ID, userId));
      if (errors != null) return errors;
      Pageable pageable = PageRequest.of(Integer.parseInt(page), Integer.parseInt(offset));
      pageObj = articleRepository.findArticlesByCreateById(Long.valueOf(userId), pageable);
      articles = pageObj.getContent();
    } catch (Exception e) {
      return new ResponseEntity<>(
          new BaseErrorResponse<>(
              400, HttpStatus.BAD_REQUEST.getReasonPhrase(), new Date(), e.getMessage()),
          HttpStatus.BAD_REQUEST);
    }
    return new ResponseEntity<>(
        new ResponseWithPagination(
            200,
            SUCCESS_FETCH,
            new Date(),
            articles,
            new Pagination(page, offset, pageObj.getTotalPages(), pageObj.getTotalElements())),
        HttpStatus.OK);
  }

  public ResponseEntity<Object> findAllByCategoryId(String categoryId, String page, String offset) {
    List<Articles> articles;
    Page<Articles> pageObj;
    try {
      ResponseEntity<Object> errors =
          validationUtil.numberValidation(
              new ValidationModel(PAGE, page),
              new ValidationModel(OFFSET, offset),
              new ValidationModel(CATEGORY_ID, categoryId));
      if (errors != null) return errors;
      Pageable pageable = PageRequest.of(Integer.parseInt(page), Integer.parseInt(offset));
      pageObj = articleRepository.findArticlesByCategoryId(Long.valueOf(categoryId), pageable);
      articles = pageObj.getContent();
    } catch (Exception e) {
      return new ResponseEntity<>(
          new BaseErrorResponse<>(
              400, HttpStatus.BAD_REQUEST.getReasonPhrase(), new Date(), e.getMessage()),
          HttpStatus.BAD_REQUEST);
    }
    return new ResponseEntity<>(
        new ResponseWithPagination(
            200,
            SUCCESS_FETCH,
            new Date(),
            articles,
            new Pagination(page, offset, pageObj.getTotalPages(), pageObj.getTotalElements())),
        HttpStatus.OK);
  }

  public ResponseEntity<Object> findById(String id) {
    Articles article;
    try {
      ResponseEntity<Object> errors =
          validationUtil.numberValidation(new ValidationModel(ARTICLE_ID, id));
      if (errors != null) return errors;
      article = articleRepository.findById(Long.valueOf(id)).orElseThrow();
    } catch (Exception e) {
      return new ResponseEntity<>(
          new BaseErrorResponse<>(
              400, HttpStatus.BAD_REQUEST.getReasonPhrase(), new Date(), e.getMessage()),
          HttpStatus.BAD_REQUEST);
    }
    return new ResponseEntity<>(
        new BaseResponse<>(200, SUCCESS_FETCH, new Date(), article), HttpStatus.OK);
  }

  public ResponseEntity<Object> create(
      ArticleDto articleDto, BindingResult bindingResult, String userId, String categoryId) {

    ResponseEntity<Object> errors =
        validationUtil.getObjectResponseEntity(bindingResult, userId, USER_ID);
    if (errors != null) return errors;
    Users user;
    Categories category;
    try {

      user =
          userRepository
              .findById(Long.valueOf(userId))
              .orElseThrow(() -> new IllegalArgumentException("User Not Found"));
      category =
          categoryId != null
              ? categoryRepository.findById(Long.valueOf(categoryId)).orElse(null)
              : null;
    } catch (Exception e) {
      return new ResponseEntity<>(
          new BaseErrorResponse<>(
              400, HttpStatus.BAD_REQUEST.getReasonPhrase(), new Date(), e.getMessage()),
          HttpStatus.BAD_REQUEST);
    }
    Articles article = articleRepository.save(new Articles(articleDto, user, category));
    return new ResponseEntity<>(
        new BaseResponse<>(201, SUCCESS_CREATE, new Date(), article), HttpStatus.CREATED);
  }

  public ResponseEntity<Object> update(
      ArticleDto articleDto, BindingResult bindingResult, String id) {
    Articles article;
    ResponseEntity<Object> errors =
        validationUtil.getObjectResponseEntity(bindingResult, id, ARTICLE_ID);
    if (errors != null) return errors;
    try {
      Articles foundArticle = articleRepository.getById(Long.valueOf(id));
      foundArticle.setTitle(articleDto.getTitle());
      foundArticle.setDescription(articleDto.getDescription());
      article = articleRepository.save(foundArticle);
    } catch (Exception e) {
      return new ResponseEntity<>(
          new BaseErrorResponse<>(
              400, HttpStatus.BAD_REQUEST.getReasonPhrase(), new Date(), e.getMessage()),
          HttpStatus.BAD_REQUEST);
    }
    return new ResponseEntity<>(
        new BaseResponse<>(200, SUCCESS_UPDATE, new Date(), article), HttpStatus.OK);
  }

  public ResponseEntity<Object> delete(String id) {
    try {
      ResponseEntity<Object> errors =
          validationUtil.numberValidation(new ValidationModel(ARTICLE_ID, id));
      if (errors != null) return errors;
      articleRepository.deleteById(Long.valueOf(id));
    } catch (Exception e) {
      return new ResponseEntity<>(
          new BaseErrorResponse<>(
              400, HttpStatus.BAD_REQUEST.getReasonPhrase(), new Date(), e.getMessage()),
          HttpStatus.BAD_REQUEST);
    }
    return new ResponseEntity<>(
        new BaseResponse<>(200, SUCCESS_DELETE, new Date(), HttpStatus.OK.getReasonPhrase()),
        HttpStatus.OK);
  }

  public ResponseEntity<Object> search(
      String articleTitle,
      String userName,
      String categoryName,
      String articleDescription,
      String categoryId,
      String userId,
      String page,
      String offset) {
    ResponseEntity<Object> errors =
        validationUtil.numberValidation(
            new ValidationModel(CATEGORY_ID, categoryId),
            new ValidationModel(USER_ID, userId),
            new ValidationModel(PAGE, page),
            new ValidationModel(OFFSET, offset));
    if (errors != null) return errors;
    List<ArticleSpecification> specs = new ArrayList<>();
    specs =
        validationUtil.specListGenerator(
            specs,
            "uname:" + userName,
            "title:" + articleTitle,
            "cname:" + categoryName,
            "dec:" + articleDescription,
            "cid:" + categoryId,
            "uid:" + userId);
    Specification<Articles> result = specs.isEmpty() ? null : specs.get(0);
    if (!specs.isEmpty()) {
      for (int i = 1; i < specs.size(); i++) {
        result =
            validationUtil.isUsingAndCriteria(specs.get(i).getValue())
                ? Specification.where(result).and(specs.get(i))
                : Specification.where(result).or(specs.get(i));
      }
    }
    List<Articles> articles;
    Page<Articles> pageObj;
    try {
      Pageable pageable = PageRequest.of(Integer.parseInt(page), Integer.parseInt(offset));
      pageObj = articleRepository.findAll(result, pageable);
      articles = pageObj.getContent();
    } catch (Exception e) {
      return new ResponseEntity<>(
          new BaseErrorResponse<>(
              400, HttpStatus.BAD_REQUEST.getReasonPhrase(), new Date(), e.getMessage()),
          HttpStatus.BAD_REQUEST);
    }
    return new ResponseEntity<>(
        new ResponseWithPagination(
            200,
            SUCCESS_FETCH,
            new Date(),
            articles,
            new Pagination(page, offset, pageObj.getTotalPages(), pageObj.getTotalElements())),
        HttpStatus.OK);
  }
}
