package com.voth.jpahomework.service;

import com.voth.jpahomework.config.security.jwt.JwtUtils;
import com.voth.jpahomework.config.security.jwtservice.UserDetailsImpl;
import com.voth.jpahomework.model.Roles;
import com.voth.jpahomework.model.Users;
import com.voth.jpahomework.model.ValidationModel;
import com.voth.jpahomework.model.dto.UserDto;
import com.voth.jpahomework.model.payload.Pagination;
import com.voth.jpahomework.model.response.BaseErrorResponse;
import com.voth.jpahomework.model.response.BaseResponse;
import com.voth.jpahomework.model.response.LogInResponse;
import com.voth.jpahomework.model.response.ResponseWithPagination;
import com.voth.jpahomework.repository.RoleRepository;
import com.voth.jpahomework.repository.UserRepository;
import com.voth.jpahomework.utils.ValidationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import java.util.Date;
import java.util.List;

@Service
public class UserService {

  private static final String SUCCESS_FETCH = "User Retrieved SuccessFully";
  private static final String SUCCESS_UPDATE = "User Updated SuccessFully";
  private static final String SUCCESS_DELETE = "User Deleted SuccessFully";
  private static final String SUCCESS_CREATE = "User Created SuccessFully";
  private static final String OFFSET = "offset";
  private static final String PAGE = "page";
  private static final String USER_ID = "User's ID";
  private static final String ROLE_ID = "Role's ID";
  private final UserRepository userRepository;
  private final ValidationUtil validationUtil;
  private final RoleRepository roleRepository;
  private final AuthenticationManager authenticationManager;
  private final JwtUtils jwtUtils;
  private final PasswordEncoder encoder;

  @Autowired
  public UserService(
      UserRepository userRepository,
      ValidationUtil validationUtil,
      RoleRepository roleRepository,
      AuthenticationManager authenticationManager,
      JwtUtils jwtUtils,
      PasswordEncoder encoder) {
    this.userRepository = userRepository;
    this.validationUtil = validationUtil;
    this.roleRepository = roleRepository;
    this.authenticationManager = authenticationManager;
    this.jwtUtils = jwtUtils;
    this.encoder = encoder;
  }

  public ResponseEntity<Object> findAll(String page, String offset) {
    List<Users> users;
    Page<Users> pageObj;
    try {
      ResponseEntity<Object> errors =
          validationUtil.numberValidation(
              new ValidationModel(PAGE, page), new ValidationModel(OFFSET, offset));
      if (errors != null) return errors;
      Pageable pageable = PageRequest.of(Integer.parseInt(page), Integer.parseInt(offset));
      pageObj = userRepository.findAll(pageable);
      users = pageObj.getContent();
    } catch (Exception e) {
      return new ResponseEntity<>(
          new BaseErrorResponse<>(
              400, HttpStatus.BAD_REQUEST.getReasonPhrase(), new Date(), e.getMessage()),
          HttpStatus.BAD_REQUEST);
    }
    return new ResponseEntity<>(
        new ResponseWithPagination(
            200,
            SUCCESS_FETCH,
            new Date(),
            users,
            new Pagination(page, offset, pageObj.getTotalPages(), pageObj.getTotalElements())),
        HttpStatus.OK);
  }

  public ResponseEntity<Object> findById(String id) {
    Users user;
    try {
      ResponseEntity<Object> errors =
          validationUtil.numberValidation(new ValidationModel(USER_ID, id));
      if (errors != null) return errors;
      user = userRepository.findById(Long.valueOf(id)).orElseThrow();
    } catch (Exception e) {
      return new ResponseEntity<>(
          new BaseErrorResponse<>(
              400, HttpStatus.BAD_REQUEST.getReasonPhrase(), new Date(), e.getMessage()),
          HttpStatus.BAD_REQUEST);
    }
    return new ResponseEntity<>(
        new BaseResponse<>(200, SUCCESS_FETCH, new Date(), user), HttpStatus.OK);
  }

  public ResponseEntity<Object> create(
      UserDto userDto, BindingResult bindingResult, String roleId) {
    ResponseEntity<Object> errors1 = validationUtil.getObjectResponseEntity(bindingResult, userDto);
    if (errors1 != null) return errors1;
    Roles role;
    try {
      role = roleRepository.findById(Long.valueOf(roleId)).orElseThrow();
    } catch (Exception e) {
      return new ResponseEntity<>(
          new BaseErrorResponse<>(
              400, HttpStatus.BAD_REQUEST.getReasonPhrase(), new Date(), "Role Not Found"),
          HttpStatus.BAD_REQUEST);
    }
    Users user =
        userRepository.save(new Users(userDto, role, encoder.encode(userDto.getPassword())));
    return new ResponseEntity<>(
        new BaseResponse<>(201, SUCCESS_CREATE, new Date(), user), HttpStatus.CREATED);
  }

  public ResponseEntity<Object> update(UserDto userDto, BindingResult bindingResult, String id) {
    ResponseEntity<Object> errors =
        validationUtil.getObjectResponseEntity(bindingResult, id, USER_ID);
    if (errors != null) return errors;
    Users user;
    try {
      Users foundUser = userRepository.getById(Long.valueOf(id));
      foundUser.setUsername(userDto.getUsername());
      foundUser.setPassword(userDto.getPassword());
      user = userRepository.save(foundUser);
    } catch (Exception e) {
      return new ResponseEntity<>(
          new BaseErrorResponse<>(
              400, HttpStatus.BAD_REQUEST.getReasonPhrase(), new Date(), e.getMessage()),
          HttpStatus.BAD_REQUEST);
    }
    return new ResponseEntity<>(
        new BaseResponse<>(200, SUCCESS_UPDATE, new Date(), user), HttpStatus.OK);
  }

  public ResponseEntity<Object> delete(String id) {
    try {
      ResponseEntity<Object> errors =
          validationUtil.numberValidation(new ValidationModel(USER_ID, id));
      if (errors != null) return errors;
      userRepository.deleteById(Long.valueOf(id));
    } catch (Exception e) {
      return new ResponseEntity<>(
          new BaseErrorResponse<>(
              400, HttpStatus.BAD_REQUEST.getReasonPhrase(), new Date(), e.getMessage()),
          HttpStatus.BAD_REQUEST);
    }
    return new ResponseEntity<>(
        new BaseResponse<>(200, SUCCESS_DELETE, new Date(), HttpStatus.OK.getReasonPhrase()),
        HttpStatus.OK);
  }

  public ResponseEntity<Object> findByRoleId(String roleId, String page, String offset) {
    List<Users> users;
    Page<Users> pageObj;
    try {
      ResponseEntity<Object> errors =
          validationUtil.numberValidation(
              new ValidationModel(PAGE, page),
              new ValidationModel(OFFSET, offset),
              new ValidationModel(ROLE_ID, roleId));
      if (errors != null) return errors;
      Pageable pageable = PageRequest.of(Integer.parseInt(page), Integer.parseInt(offset));
      pageObj = userRepository.findUsersByRolesId(Long.valueOf(roleId), pageable);
      users = pageObj.getContent();
    } catch (Exception e) {
      return new ResponseEntity<>(
          new BaseErrorResponse<>(
              400, HttpStatus.BAD_REQUEST.getReasonPhrase(), new Date(), e.getMessage()),
          HttpStatus.BAD_REQUEST);
    }
    return new ResponseEntity<>(
        new ResponseWithPagination(
            200,
            SUCCESS_FETCH,
            new Date(),
            users,
            new Pagination(page, offset, pageObj.getTotalPages(), pageObj.getTotalElements())),
        HttpStatus.OK);
  }

  public ResponseEntity<Object> login(UserDto userDto, BindingResult bindingResult) {
    ResponseEntity<Object> errors = validationUtil.getObjectResponseEntity(bindingResult);
    if (errors != null) return errors;
    try {
      Authentication authentication =
          authenticationManager.authenticate(
              new UsernamePasswordAuthenticationToken(
                  userDto.getUsername(), userDto.getPassword()));

      SecurityContextHolder.getContext().setAuthentication(authentication);
      String jwt = jwtUtils.generateJwtToken(authentication);
      UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
      return new ResponseEntity<>(
          new LogInResponse(
              HttpStatus.OK.value(),
              "Successfully login",
              new Date(),
              new Users(userDetails.getId(), userDetails.getUsername(),roleRepository.findRolesByUsersId(userDetails.getId())),
              jwt),
          HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(
          new BaseErrorResponse<>(
              400, HttpStatus.BAD_REQUEST.getReasonPhrase(), new Date(), e.getMessage()),
          HttpStatus.BAD_REQUEST);
    }
  }
}
