package com.voth.jpahomework.repository;

import com.voth.jpahomework.model.Users;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<Users, Long> {

  Page<Users> findUsersByRolesId(Long roleId, Pageable pageable);

  Optional<Users> findByUsername(String username);

}
