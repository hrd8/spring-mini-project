package com.voth.jpahomework.repository;

import com.voth.jpahomework.model.Articles;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ArticleRepository
    extends JpaRepository<Articles, Long>, JpaSpecificationExecutor<Articles> {

  Page<Articles> findArticlesByCreateById(Long userId, Pageable pageable);

  Page<Articles> findArticlesByCategoryId(Long categoryId, Pageable pageable);
}
