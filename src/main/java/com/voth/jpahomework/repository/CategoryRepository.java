package com.voth.jpahomework.repository;

import com.voth.jpahomework.model.Categories;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CategoryRepository extends JpaRepository<Categories,Long> {

    Optional<Categories> findByName(String name);
}
