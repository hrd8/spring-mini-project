package com.voth.jpahomework.repository.criteria;

import com.voth.jpahomework.model.Articles;
import com.voth.jpahomework.model.Categories;
import com.voth.jpahomework.model.Users;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;


/**
 * Specification class of type Article<br/>
 * Default pattern is 'field name : value' ex: uid:1 this will create predicate to test for
 * equality ('=' in sql)
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ArticleSpecification implements Specification<Articles> {
  private String value;

  /**
   * Default pattern is 'field name : value' ex: uid:1 this will create predicate to test for
   * equality ('=' in sql)
   *
   * @return predicate with the given pattern
   */
  @Override
  public Predicate toPredicate(
      Root<Articles> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
    String[] split = value.split(":");
    Join<Articles, Users> userJoin = root.join("user", JoinType.LEFT);
    Join<Articles, Categories> categoriesJoin = root.join("category", JoinType.LEFT);
    switch (split[0]) {
      case "uid":
        return criteriaBuilder.equal(userJoin.get("id"), Long.valueOf(split[1]));
      case "cid":
        return criteriaBuilder.equal(categoriesJoin.get("id"), Long.valueOf(split[1]));
      case "cname":
        return criteriaBuilder.like(categoriesJoin.get("name"), "%" + split[1] + "%");
      case "title":
        return criteriaBuilder.like(root.get("title"), "%" + split[1] + "%");
      case "dec":
        return criteriaBuilder.like(root.get("description"), "%" + split[1] + "%");
      case "uname":
        return criteriaBuilder.like(userJoin.get("username"), "%" + split[1] + "%");
      default:
        return null;
    }
  }
}
