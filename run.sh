#!/bin/sh
ls -l build/libs/ | awk '{print $9}' | while read -r plainFile;do if [[ $plainFile =~ "plain" ]];then rm -rf "build/libs/$plainFile"; fi; done;
echo 'FROM adoptopenjdk/openjdk11' > Dockerfile
echo 'ARG JAR_FILE=build/libs/*.jar' >> Dockerfile
echo 'COPY ${JAR_FILE} app.jar' >> Dockerfile
echo 'ENTRYPOINT ["java","-jar","/app.jar"]' >> Dockerfile
docker build -t stvoth/mn-project-one .
docker stop mn-project-one
docker rm mn-project-one
docker service ls
#docker run -d -p 8083:8080 --name mn-project-one stvoth/mn-project-one
docker service update --image stvoth/mn-project-one:latest mn-project-one
docker service ls
